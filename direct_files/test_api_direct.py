import requests, json
from datetime import datetime, timedelta

# date
start_date = datetime.strftime(datetime.today() - timedelta(days = 1), "%Y-%m-%d")
end_date = datetime.strftime(datetime.now(), "%Y-%m-%d")

#  Метод для корректной обработки строк в кодировке UTF-8 как в Python 3, так и в Python 2
import sys

if sys.version_info < (3,):
    def u(x):
        try:
            return x.encode("utf8")
        except UnicodeDecodeError:
            return x
else:
    def u(x):
        if type(x) == type(b''):
            return x.decode('utf8')
        else:
            return x

# --- Входные данные ---
#  Адрес сервиса Campaigns для отправки JSON-запросов (регистрозависимый)
ReportsURL = 'https://api.direct.yandex.com/json/v5/reports'

# OAuth-токен пользователя, от имени которого будут выполняться запросы
token = 'AQAAAAAjeVqfAAMfZQ7FYbIyrkFBkdkkD0ANkUo'

# Логин клиента рекламного агентства
# Обязательный параметр, если запросы выполняются от имени рекламного агентства
clientLogin =  'morozova26-ru'

# --- Подготовка, выполнение и обработка запроса ---
#  Создание HTTP-заголовков запроса
headers = {"Authorization": "Bearer " + token,  # OAuth-токен. Использование слова Bearer обязательно
           "Client-Login": clientLogin,  # Логин клиента рекламного агентства
           "Accept-Language": "ru",  # Язык ответных сообщений
           "processingMode": "auto"
            }

# Создание тела запроса
body = {
    "params": {
        "SelectionCriteria": {
            "DateFrom": start_date,
            "DateTo": start_date
        },
        "FieldNames": [
            "Date",
            "CampaignName",
            "LocationOfPresenceName",
            "Impressions",
            "Clicks",
            "Cost"
        ],
        "ReportName": u("direct_reports10"),
        "ReportType": "CAMPAIGN_PERFORMANCE_REPORT",
        "DateRangeType": "CUSTOM_DATE",
        "Format": "TSV",
        "IncludeVAT": "NO",
        "IncludeDiscount": "NO"
    }
}
# Кодирование тела запроса в JSON
body = json.dumps(body, indent=4)


# Выполнение запроса
req = requests.post(ReportsURL, body, headers=headers)
req.encoding = 'utf-8'


print("RequestId: {}".format(req.headers.get("RequestId", False)))
print("Содержание отчета: \n{}".format(u(req.text)))

