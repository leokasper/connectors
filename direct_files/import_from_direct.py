# Импортирует данные из Yandex.Direct в BigQuery
# Типичная ошибка list index out of range. возвращает пустой массив, 
# Исправление: изменение даты  получения отчета в меньшую сторону, скорее всего стоит ограничение
# на колличество дней   

import requests
import json
import sys
from datetime import datetime, timedelta
from google.cloud import bigquery


# date
start_date = datetime.strftime(datetime.today() - timedelta(days = 1), "%Y-%m-%d")
end_date = datetime.strftime(datetime.now(), "%Y-%m-%d")

#  Метод для корректной обработки строк в кодировке UTF-8 как в Python 3, так и в Python 2


if sys.version_info < (3,):
    def u(x):
        try:
            return x.encode("utf8")
        except UnicodeDecodeError:
            return x
else:
    def u(x):
        if type(x) == type(b''):
            return x.decode('utf8')
        else:
            return x

# --- Входные данные ---
#  Адрес сервиса Campaigns для отправки JSON-запросов (регистрозависимый)
ReportsURL = 'https://api.direct.yandex.com/json/v5/reports'

# OAuth-токен пользователя, от имени которого будут выполняться запросы
token = ''

# Логин клиента рекламного агентства
# Обязательный параметр, если запросы выполняются от имени рекламного агентства
clientLogin =  '-ru'

# --- Подготовка, выполнение и обработка запроса ---
#  Создание HTTP-заголовков запроса
headers = {"Authorization": "Bearer " + token,  # OAuth-токен. Использование слова Bearer обязательно
           "Client-Login": clientLogin,  # Логин клиента рекламного агентства
           "Accept-Language": "ru",  # Язык ответных сообщений
           "processingMode": "auto",
           "returnMoneyInMicros": "false"
            }

# Создание тела запроса
# DateFrom и DateTo указываються даты , далее планируеться 
# получение данных из аргуметов sys.args
body = {
    "params": {
        "SelectionCriteria": {
            "DateFrom": '2019-01-01', 
            "DateTo": '2019-01-09'
        },
        "FieldNames": [
            "CampaignName",
            "Date",           
            "Impressions",
            "Clicks",
            "Cost",
            "AvgCpc",
            "Ctr"
            
           
        ],
        "ReportName": u("direct_reports"),
        "ReportType": "CAMPAIGN_PERFORMANCE_REPORT",
        "DateRangeType": "CUSTOM_DATE",
        "Format": "TSV",
        "IncludeVAT": "NO",
        "IncludeDiscount": "NO"
    }
}
# Кодирование тела запроса в JSON
body = json.dumps(body, indent=4)
print()

# Выполнение запроса
req = requests.post(ReportsURL, body, headers=headers)
req.encoding = 'utf-8'


print("RequestId: {}".format(req.headers.get("RequestId", False)))
print("Содержание отчета: \n{}".format(u(req.text)))

# Дополнительные поля     
#info_column=",id_client,id_service"
#info_val=",1,1"

# Создание файла , запись результата запроса и форматирование 
# При переносе требуеться указать полный путь к .csv 
file=open("D:\Morozova26\direct_files\.csv","w",encoding='utf-8')
file.write(req.text.split("\n")[1].replace("\t",",")+"\n")
for line in req.text.split("\n")[2:-2]:
    file.write(line.replace("\t",",").replace("--","0")+"\n")

# Закрытие файла для сохранения
file.close()

# Подключение к BQ, вход в акканут через JSON, указывается база данных и таблица
# При переносе требуеться указать полный путь к alm-athlete-205911-7c383a305723.json  
client = bigquery.Client.from_service_account_json('')
dataset_ref = client.dataset('')
table_ref = dataset_ref.table('data_direct')
table = client.get_table(table_ref)

# Настройка таблицы BQ для загрузки 
job_config = bigquery.LoadJobConfig()
job_config.write_disposition = bigquery.WriteDisposition.WRITE_APPEND #WRITE_TRUNCATE and WRITE_APPEND    
job_config.source_format = bigquery.SourceFormat.CSV
job_config.max_bad_records=0
#job_config.autodetect=True
job_config.skip_leading_rows = 1
    
# Запись в BQ с помощью созданного файла
# При переносе требуеться указать полный путь к .csv 
newfile=open("","rb")
load_job=client.load_table_from_file(newfile,
    table_ref,
    job_config=job_config)
print(load_job.result())      # Waits for table load to complete.

# Закрытие файла для сохранения
newfile.close()