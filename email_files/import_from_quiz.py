# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 10:34:24 2018

@author: COSMOS-1
"""
import re
import glob
import base64
import codecs
import imaplib
import email
from google.cloud import bigquery

def get_first_text_block(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.get_payload():
            if part.get_content_maintype() == 'text':
                #file.write(part.get_payload())
                return part.get_payload()
    elif maintype == 'text':
        return email_message_instance.get_payload()

def format_str(string: str) -> str:
    return string.replace("\n", "").replace("\t", "").replace("\r", "").replace("\r\n", "").replace(",", "") #.replace(" ","")

mail = imaplib.IMAP4_SSL('imap.mail.ru')
mail.login('', '')
file = codecs.open("output.csv",'w+',"utf_8_sig")
temp = r"[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]"
file.writelines('"flat_area","budget","payment","floor","excursion","name","phone","client_id","date"\n')
mail.list()
status, count = mail.select("inbox") #&BCEEPwQwBDw- spam\


result, data = mail.uid('search', None, "ALL") # Выполняет поиск и возвращает UID писем.
list_email_uid = data[0].split()


for uid in list_email_uid:
    result, data = mail.uid('fetch', uid, '(RFC822)')
    raw_email = data[0][1]
    email_message = email.message_from_bytes(raw_email)
    s = get_first_text_block(email_message)
    if email.utils.parseaddr(email_message['From'])[1] == "": #noreply@unverified.beget.ru
        for line in s.split("<br>"):           
            #file.write(line)
            if format_str(line).split(":")[0] == "" :
                print(type(line))
            else:
                #print(format_str(line).split(":")[1])
                file.writelines("\"" + format_str(line).split(":")[1] + "\"" + ',')   
                
        file.writelines(str(list(email.header.decode_header(email_message['subject'])[0])[0].split()[-2]).replace('b','').replace('\'','\"') +  "\n")

file.close() 

schema = [
        bigquery.SchemaField('flat_area', 'STRING', mode='nullable'),
        bigquery.SchemaField('budget', 'STRING', mode='nullable'),
        bigquery.SchemaField('payment', 'STRING', mode='nullable'),
        bigquery.SchemaField('floor', 'STRING', mode='nullable'),
        bigquery.SchemaField('excursion', 'STRING', mode='nullable'),
        bigquery.SchemaField('name', 'STRING', mode='nullable'),
        bigquery.SchemaField('phone', 'STRING', mode='nullable'),
        bigquery.SchemaField('client_id', 'STRING', mode='nullable'),
        bigquery.SchemaField('date', 'DATE', mode='nullable'),
     ]


client = bigquery.Client.from_service_account_json('')
dataset_ref = client.dataset('')
table_ref = dataset_ref.table('data_quiz')
table = client.get_table(table_ref)

#
job_config = bigquery.LoadJobConfig()
job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE #WRITE_TRUNCATE and WRITE_APPEND    
job_config.source_format = bigquery.SourceFormat.CSV
job_config.max_bad_records= 8 #len(count)
#job_config.autodetect=True
job_config.field_delimiter=","
    
#its work (file)
namefile = glob.glob("*.csv")[0]
newfile=open(namefile, 'rb')
load_job=client.load_table_from_file(newfile,
    table_ref,
    job_config=job_config)
load_job.result()  # Waits for table load to complete.


