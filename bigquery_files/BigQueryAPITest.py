from google.cloud import bigquery
from google.cloud import storage
from google.auth import compute_engine


def main():
    #client=bigquery.Client(project='calm-athlete-205911')
    client = bigquery.Client.from_service_account_json(
        'D:\\Morozova26\\bigquery_files\\calm-athlete-205911-7c383a305723.json')
    #buckets = list(client.list_buckets())
    #print(buckets)
    
    dataset_ref = client.dataset('data_calltouch')
    table_ref = dataset_ref.table('table_CRM_csv_test_v5')
    table = client.get_table(table_ref)
    
    for val in table.schema:
        print(val._name)
    
if __name__=='__main__':
    main()
