# -*- coding: utf-8 -*-
import glob, os, sys
import codecs
from google.cloud import bigquery

"""
Created on Thu Oct  4 12:56:30 2018

@author: COSMOS-1
"""

#line='"nametransaction";"budget";"responsible";"deal_date";"created_deal";"editing_date";"edited_by";"tags";"note";"note_2";"note_3";"note_4";"note_5";"deal_stage";"contact_full_name";"contact_company";"responsible_contact";"company";"work_phone";"working_direct_telephone";"mobile_phone";"fax";"home_phone";"other_phone";"work_email";"personal_email";"other_email";"created_from";"ID_client_Calltouch_contact";"position";"source_contact";"Skype";"ICQ";"Jabber";"Google_Talk";"MSN";"other_IM";"number_apartment";"residential_complex";"home";"entrance";"floor";"number_room";"metering";"price_metering";"ID_room";"created_from";"client_number";"forwarding_number";"tracking_number";"date_application";"time_circulation";"duration";"expectation";"download_entry";"customer_name";"Email";"application_subject";"comment_application";"source_utm_source";"channel_utm_medium";"campaign_utm_campaign";"ad_utm_content";"keyword_query_utm_term";"tracked_site";"site_login_page";"referral_jump_page";"execution_page";"city";"operating_system";"browser";"ID_Calltouch";"ID_applications";"ID_ATS";"ID_session";"ID_client";"google_client_ID";"yandex_client_ID";"type_treatment";"moment_sending";"pool_type";"call_status";"ring_type";"claim_type";"attribution_model";"device"\n'
#'"nametransaction";"budget";"responsible";"deal_date";"created_deal";"editing_date";"edited_by";"tags";"note";"note_2";"note_3";"note_4";"note_5";"deal_stage";"contact_full_name";"contact_company";"responsible_contact";"company";"work_phone";"working_direct_telephone";"mobile_phone";"fax";"home_phone";"other_phone";"work_email";"personal_email";"other_email";"position";"Skype";"ICQ";"Jabber";"Google_Talk";"MSN";"other_IM";"number_apartment";"residential_complex";"home";"entrance";"floor";"number_room";"metering";"price_metering";"ID_room";"created_from";"client_number";"forwarding_number";"tracking_number";"date_application";"time_circulation";"duration";"expectation";"download_entry";"customer_name";"Email";"application_subject";"comment_application";"source_utm_source";"channel_utm_medium";"campaign_utm_campaign";"ad_utm_content";"keyword_query_utm_term";"tracked_site";"site_login_page";"referral_jump_page";"execution_page";"city";"operating_system";"browser";"ID_Calltouch";"ID_applications";"ID_ATS";"ID_session";"ID_client";"google_client_ID";"yandex_client_ID";"type_treatment";"moment_sending";"pool_type";"call_status";"ring_type";"claim_type";"attribution_model";"device"\n'
path=''

os.chdir(path)
name_file=glob.glob("*leads*.csv")[0]

print(path+name_file)
#'D:/Morozova26/ammocrm_files/amocrm_leads (5).csv'
file=codecs.open(path+name_file,'r', "utf_8_sig")
#print(file.readline())
format_file=codecs.open(path+"\.csv",'w', "utf_8_sig")
#format_file.writelines(line)
for line in file.read().split("\n")[1:]:
    format_file.writelines(line.replace("+","").replace("-","")+"\n")
format_file.close()


client = bigquery.Client.from_service_account_json('')
dataset_ref = client.dataset('')
table_ref = dataset_ref.table('data_ammocrm')
table = client.get_table(table_ref)

#
job_config = bigquery.LoadJobConfig()
job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE #WRITE_TRUNCATE and WRITE_APPEND    
job_config.source_format = bigquery.SourceFormat.CSV
job_config.max_bad_records=0
job_config.autodetect=True
job_config.field_delimiter=";"
    
#its work (file)
newfile=open(path+"\.csv",'rb')
load_job=client.load_table_from_file(newfile,
    table_ref,
    job_config=job_config)
load_job.result()  # Waits for table load to complete.