
from facebookads.adobjects.adaccount import AdAccount
from facebookads.adobjects.adsinsights import AdsInsights
from facebookads.api import FacebookAdsApi

access_token = ''
ad_account_id = ''
app_secret = ''
app_id = ''
FacebookAdsApi.init(access_token=access_token)

fields = [
	'spend',
	'clicks',
	'campaign_group_name']
params = {
	'level': 'ad',
	'filtering': [],
	'breakdowns': ['days_1'],
	'time_range': {'since':'2018-10-01','until':'2018-10-20'},
	}
print (AdAccount(ad_account_id).get_insights(
		fields=fields,
		params=params,
	))


