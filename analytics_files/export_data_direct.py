# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 18:03:44 2019

@author: COSMOS-1

"""

import requests
import json
import sys
from datetime import datetime, timedelta
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from apiclient.http import MediaFileUpload

#  Метод для корректной обработки строк в кодировке UTF-8 как в Python 3, так и в Python 2
if sys.version_info < (3,):
    def u(x):
        try:
            return x.encode("utf8")
        except UnicodeDecodeError:
            return x
else:
    def u(x):
        if type(x) == type(b''):
            return x.decode('utf8')
        else:
            return x

scope = 'https://www.googleapis.com/auth/analytics.edit'
key_file_location = ''
api_name='analytics'
api_version='v3'

credentials = ServiceAccountCredentials.from_json_keyfile_name(
            key_file_location, scopes=scope)

# Build the service object.
service = build(api_name, api_version, credentials=credentials)
accounts = service.management().accounts().list()


profile_id = accounts.execute().get("items")[1].get("id")

# date
start_date = datetime.strftime(datetime.today() - timedelta(days = 1), "%Y-%m-%d")
end_date = datetime.strftime(datetime.now(), "%Y-%m-%d")

# --- Входные данные ---
#  Адрес сервиса Campaigns для отправки JSON-запросов (регистрозависимый)
ReportsURL = 'https://api.direct.yandex.com/json/v5/reports'

# OAuth-токен пользователя, от имени которого будут выполняться запросы
token = ''

# Логин клиента рекламного агентства
# Обязательный параметр, если запросы выполняются от имени рекламного агентства
clientLogin =  'morozova26-ru'

# --- Подготовка, выполнение и обработка запроса ---
#  Создание HTTP-заголовков запроса
headers = {"Authorization": "Bearer " + token,  # OAuth-токен. Использование слова Bearer обязательно
           "Client-Login": clientLogin,  # Логин клиента рекламного агентства
           "Accept-Language": "ru",  # Язык ответных сообщений
           "processingMode": "auto",
           "returnMoneyInMicros": "false"
            }

# Создание тела запроса
# DateFrom и DateTo указываються даты , далее планируеться 
# получение данных из аргуметов sys.args
body = {
    "params": {
        "SelectionCriteria": {
            "DateFrom":  '2019-01-01', #start_date,
            "DateTo":  '2019-01-23' #start_date
        },
        "FieldNames": [
            "Date",                                
            "Cost",
            "Clicks",
            "Impressions",
            "CampaignId",   
                                          
        ],
        "ReportName": u("direct_reports"),
        "ReportType": "CAMPAIGN_PERFORMANCE_REPORT",
        "DateRangeType": "CUSTOM_DATE",
        "Format": "TSV",
        "IncludeVAT": "NO",
        "IncludeDiscount": "NO"
    }
}
# Кодирование тела запроса в JSON
body = json.dumps(body, indent=4)
print()

# Выполнение запроса
req = requests.post(ReportsURL, body, headers=headers)
req.encoding = 'utf-8'


print("RequestId: {}".format(req.headers.get("RequestId", False)))
print("Содержание отчета: \n{}".format(u(req.text)))



file=open(r"","w",encoding='utf-8')
file.write("ga:date,ga:medium,ga:source,ga:adCost,ga:adClicks,ga:impressions,ga:campaign\n")
for line in req.text.split("\n")[2:-2]:
    column = line.split("\t")
    date = column[0].replace("-","")
    camp = column[4]
    impr = column[3]
    clicks = column[2]
    cost = column[1]
    format_line = '{0},cpc,yandex,{1},{2},{3},{4}\n'.format(date,cost,clicks,impr,camp)
    file.write(format_line.replace("\t",",").replace("--","0"))
# Закрытие файла для сохранения
file.close()

media = MediaFileUpload(r"",
                          mimetype='application/octet-stream',
                          resumable=False)

daily_upload = service.management().uploads().uploadData(
      accountId='130948208',
      webPropertyId='UA-130948208-1',
      customDataSourceId='iS9K_Vb5TTGGvPjuW30lkA',
      media_body=media).execute()

daily_list = service.management().uploads().list(accountId='130948208',
      webPropertyId='UA-130948208-1',
      customDataSourceId='iS9K_Vb5TTGGvPjuW30lkA').execute()

print(daily_upload)
print(daily_list)