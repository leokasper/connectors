# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 13:53:08 2019

@author: COSMOS-1
"""

import json
from datetime import datetime, timedelta
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

scope = 'https://www.googleapis.com/auth/analytics.readonly'
key_file_location = ''
api_name='analytics'
api_version='v3'



def get_service(api_name, api_version, scopes, key_file_location):

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            key_file_location, scopes=scopes)

    # Build the service object.
    service = build(api_name, api_version, credentials=credentials)

    return service

service = get_service(
            api_name='analytics',
            api_version='v3',
            scopes=[scope],
            key_file_location=key_file_location)
            
daily_upload = service.data().mcf().get(
      ids ="ga:186627820" , #+ profile_id
        start_date = '2019-01-21',
        end_date = '2019-01-21',
      metrics = 'mcf:totalConversions',
    dimensions = 'mcf:basicChannelGroupingPath,mcf:conversionDate,mcf:pathLengthInInteractionsHistogram,mcf:campaignPath').execute()
      
d = daily_upload['rows']


def count_path(c):
    d = dict()
    for val in set(c):
        d[val] = 0
        for x in c:
            if val == x:
                d[val] = d[val] + 1
    return d            
'''
for val in d:  
    c = list()
    for x in val[0]:
        goal = val[-1]        
        for y in val[0][x]:
            c.append(y['nodeValue'])
            d = count_path(c)
            m = ''
            for  key in d :
                m = m + ('{0} x{1} '.format(key,d[key]))
        print(m)
'''
file = open("mcf-test.csv",'w+')

for val in d:
    date = val[1] 
    camp = val[-2]['conversionPathValue'][0]['nodeValue']
    c = list()    
    for x in val[0]:
        goal = val[-1] 
        print(val[0][x][0]['nodeValue'])    
        for y in val[0][x]:
            c.append(y['nodeValue'])
            d = count_path(c)
            m = ''
            for  key in d :
                m = m + ('{0} x{1} '.format(key,d[key]))
        file.write('{0},{1},{2},{3}'.format(m,goal['primitiveValue'],date['primitiveValue'],camp)+'\n')
file.close()            