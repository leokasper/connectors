# -*- coding: utf-8 -*- 
import requests
import json
#
#
atoken = ''
id_c =  ''
date_from = '2019-01-11' #"2018-09-01"
date_to = '2019-01-15' #"2018-10-01"
#
#
#{4}
dimensions = "ym:s:clientID,ym:s:regionCity,ym:s:UTMCampaign,ym:s:date" #ym:s:UTMCampaign"
#{5}
metrics =   'ym:s:users' #"ym:s:visits,ym:s:users"
#{6}
sortby =    "ym:s:clientID"#"ym:s:date"
#
#
url ='https://api-metrika.yandex.ru/stat/v1/data?&id={0}&accuracy=full&date1={1}&date2={2}&group=day&dimensions={4}&metrics={5}&sort={6}&oauth_token={3}'.format(id_c,date_from,date_to,atoken,dimensions,metrics,sortby)

r = requests.get(url)
parsed = json.loads(r.text)

rec = str(json.dumps(parsed, indent=4, sort_keys=True))
print(rec.encode().decode('utf8'))

file = open('data_metrica.csv', "w+")

for day in parsed['data']:
    file.write(str(day['dimensions'][0]['name']).encode().decode("UTF-8") + ',' +
           str(day['dimensions'][1]['name']).encode().decode("UTF-8") + ',' + 
           str(day['dimensions'][2]['name']).encode().decode("UTF-8") + ',' + 
           str(day['dimensions'][3]['name']).encode().decode("UTF-8") + ',' + 
           str(day['metrics'][0]) + '\n' 
        #  str(day['metrics'][1]) + '\t' 
   #        str(day['metrics'][2]) 
           )

file.close()


