"""A simple example of how to access the Google Analytics API."""

import sys
from datetime import datetime, timedelta
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from google.cloud import bigquery

#arg
args = sys.argv

# date
start_date = datetime.strftime(datetime.today() - timedelta(days = 1), "%Y-%m-%d")
end_date = datetime.strftime(datetime.now(), "%Y-%m-%d")


def get_service(api_name, api_version, scopes, key_file_location):

    credentials = ServiceAccountCredentials.from_json_keyfile_name(
            key_file_location, scopes=scopes)

    # Build the service object.
    service = build(api_name, api_version, credentials=credentials)

    return service


def get_results(service, profile_id):
    # Use the Analytics Service Object to query the Core Reporting API
    # for the number of sessions within the past seven days.
    return service.data().ga().get(
            ids='ga:' + profile_id,
            start_date = '2019-01-01', #7daysAgo
            end_date = '2019-03-21',   
            metrics='ga:adCost,ga:adClicks,ga:impressions,ga:CPC,ga:CPM,ga:CTR,ga:ROAS',
# append "ga:city",
            dimensions='ga:campaign,ga:date,ga:adwordsCampaignID,ga:adGroup').execute()


def print_results(results):
    # Print data nicely for the user.
    if results:
        print('View (Profile):', results.get('profileInfo').get('profileName'))
        print(results.get('rows'))
        

    else:
        print('No results found')
        
def rec_data(file):    
    #    
    client = bigquery.Client.from_service_account_json('')
    dataset_ref = client.dataset('')
    table_ref = dataset_ref.table('data_adwords')
    table = client.get_table(table_ref)
    #
    job_config = bigquery.LoadJobConfig()
    job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE #WRITE_TRUNCATE and WRITE_APPEND    
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.max_bad_records=0
    job_config.autodetect=True
    
    #its work (file)
    newfile=file
    load_job=client.load_table_from_file(newfile,
    table_ref,
    job_config=job_config)
    print(load_job.result())  # Waits for table load to complete.

# append "City",       
def format_data(file,result):
    first_line='"CampaignName","Date","adwordsCampaignID","Group","Cost","Clicks","Impressions","CPC","CPM","CTR","ROAS"\n'
    file.write(first_line)
    for line in result:
        date=str(line[0][:4])+"-"+str(line[0][4:6])+"-"+str(line[0][6:8])
        line[0]=date
        file.write(str(line).replace('\"', '').replace('[','').replace(']','').replace('\'','\"')+'\n')        
    file.close()    
def main():
    # Define the auth scopes to request.
    scope = 'https://www.googleapis.com/auth/analytics.readonly'
    key_file_location = ''

    # Authenticate and construct service.
    service = get_service(
            api_name='analytics',
            api_version='v3',
            scopes=[scope],
            key_file_location=key_file_location)

    profile_id = ''
    result=get_results(service, profile_id)
    print_results(result)
    
    file=open(r'','w+',encoding='utf-8')
    format_data(file,result.get('rows'))
    
    file=open(r'','rb')
    rec_data(file)
    print(file.read())

if __name__ == '__main__':
    main()
